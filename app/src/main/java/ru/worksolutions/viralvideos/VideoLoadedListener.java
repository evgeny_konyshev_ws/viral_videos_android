package ru.worksolutions.viralvideos;

public interface VideoLoadedListener {
    void onVideoLoaded(VideoItem videoItem);
}
