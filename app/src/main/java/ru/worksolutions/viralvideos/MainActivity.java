package ru.worksolutions.viralvideos;

import android.content.Context;
import android.net.ConnectivityManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.dailymotion.android.player.sdk.PlayerWebView;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import retrofit2.http.Url;

public class MainActivity extends AppCompatActivity implements VideoLoadedListener {

    private static final String LOG_TAG = MainActivity.class.getSimpleName();

    VideoAdapter videoAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        List<VideoItem> videoItems = new ArrayList<>();
        videoAdapter = new VideoAdapter(this, videoItems);

        RecyclerView recyclerView = findViewById(R.id.main_recycler);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this, layoutManager.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);

        recyclerView.setAdapter(videoAdapter);
        YoutubeVideoLoader youtubeVideoLoader = new YoutubeVideoLoader(this, this);
        youtubeVideoLoader.loadVideos();

        VimeoVideoLoader vimeoVideoLoader = new VimeoVideoLoader(this, this);
        vimeoVideoLoader.loadVideos();

        DailyMotionVideoLoader dailyMotionVideoLoader = new DailyMotionVideoLoader(this, this);
        dailyMotionVideoLoader.loadVideos();
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    @Override
    public void onVideoLoaded(VideoItem videoItem) {
        videoAdapter.add(videoItem);
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onPause() {
        super.onPause();

    }
}


