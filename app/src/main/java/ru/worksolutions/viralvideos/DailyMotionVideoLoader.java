package ru.worksolutions.viralvideos;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public class DailyMotionVideoLoader {

    private static final String LOG_TAG = DailyMotionVideoLoader.class.getSimpleName();

    private VideoLoadedListener listener;
    private Context context;
    private DailyMotionService dailyMotionService;
    private DailyMotionThumbnailService dailyMotionThumbnailService;

    public DailyMotionVideoLoader(VideoLoadedListener listener, Context context) {
        this.listener = listener;
        this.context = context;

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.dailymotion.com/")
                .build();

        dailyMotionService = retrofit.create(DailyMotionService.class);
        dailyMotionThumbnailService = retrofit.create(DailyMotionThumbnailService.class);
    }

    public interface DailyMotionService{

        @GET("/videos")
        Call<ResponseBody> load(@Query("sort") String sort);
    }

    public interface DailyMotionThumbnailService {

        @GET("/video/{videoId}")
        Call<ResponseBody> loadThumbnail(@Path("videoId") String videoId,
                                @Query("fields") String fields);
    }

    void loadVideos(){
        String sort = "trending";
//        fetch video ID
        Call<ResponseBody> call = dailyMotionService.load(sort);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.e(LOG_TAG, "Success!!!");
                Log.e(LOG_TAG, call.request().toString());
                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    JSONArray list = jsonObject.getJSONArray("list");
                    for (int i = 0; i < list.length(); i++){
                        final String videoId = list.getJSONObject(i).getString("id");
//                        fetch thumbnail for each video
                        Call<ResponseBody> callForThumbnail = dailyMotionThumbnailService.loadThumbnail(videoId, "thumbnail_360_url");
                        callForThumbnail.enqueue(new Callback<ResponseBody>() {
                            @Override
                            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                try {
                                    JSONObject jsonObject = new JSONObject(response.body().string());
                                    String thumbnailUrl = jsonObject.getString("thumbnail_360_url");
                                    listener.onVideoLoaded(new VideoItem(videoId, VIDEO_SERVICE.DAILYMOTION, thumbnailUrl));
                                } catch (JSONException | IOException e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseBody> call, Throwable t) {
                                Log.e(LOG_TAG, t.getMessage());
                            }
                        });
                    }
                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(LOG_TAG, t.getMessage());
            }
        });
    }
}
