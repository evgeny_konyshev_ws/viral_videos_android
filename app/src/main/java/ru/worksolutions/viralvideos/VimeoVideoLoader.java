package ru.worksolutions.viralvideos;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.http.GET;
import retrofit2.http.Query;

public class VimeoVideoLoader {

    private static final String LOG_TAG = VimeoVideoLoader.class.getSimpleName();

    private Context context;
    private VimeoService vimeoService;
    private VimeoEmbedService vimeoEmbedService;
    private VideoLoadedListener listener;

    public VimeoVideoLoader(Context context, VideoLoadedListener listener) {
        this.listener = listener;
        this.context = context;
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.vimeo.com/")
                .build();

        Retrofit retrofitForHtml = new Retrofit.Builder()
                .baseUrl("https://vimeo.com/")
                .build();

        vimeoService = retrofit.create(VimeoService.class);
        vimeoEmbedService = retrofitForHtml.create((VimeoEmbedService.class));
    }

    public interface VimeoService {

        @GET("videos")
        Call<ResponseBody> load(@Query("filter") String filter,
                                @Query("query") String query,
                                @Query("per_page") int perPage,
                                @Query("access_token") String token);
    }

    public interface VimeoEmbedService {

        @GET("/api/oembed.json")
        Call<ResponseBody> load(@Query("url") String jsonQueryParam,
                                @Query("width") int width,
                                @Query("height") int height);
    }

    public void loadVideos() {
        String filter = "trending";
        String query = "item";
        int perPage = 1;
        String token = context.getString(R.string.vimeo_access_token);

        Call<ResponseBody> call = vimeoService.load(filter, query, 10, token);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    JSONArray data = jsonObject.getJSONArray("data");
                    for (int i = 0; i < data.length(); i++){
                        String videoUri = data.getJSONObject(i).getString("uri");
                        String videoId = videoUri.split("/")[2];
                        String oembedQueryParam = "https://vimeo.com/" + videoId;
                        loadHtml(oembedQueryParam);
                    }
                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(LOG_TAG, call.request().toString());
                Log.e(LOG_TAG, t.getMessage());
            }
        });
    }

    private void loadHtml(String oembedQueryParam){
        Call<ResponseBody> call = vimeoEmbedService.load(oembedQueryParam, 100, 75);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    String htmlString = jsonObject.getString("html");
                    String htmlAdress = htmlString.substring(htmlString.indexOf("https"), htmlString.length() - 10);
                    listener.onVideoLoaded(new VideoItem(htmlAdress, VIDEO_SERVICE.VIMEO, null));
                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(LOG_TAG, call.request().toString());
                Log.e(LOG_TAG, t.getMessage());
            }
        });
    }

}
