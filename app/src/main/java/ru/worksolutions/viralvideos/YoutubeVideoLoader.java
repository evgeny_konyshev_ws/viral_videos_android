package ru.worksolutions.viralvideos;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.http.GET;
import retrofit2.http.Query;

public class YoutubeVideoLoader {

    private static final String LOG_TAG = YoutubeVideoLoader.class.getSimpleName();

    private YoutubeService youtubeService;
    private VideoLoadedListener listener;
    private Context context;

    public YoutubeVideoLoader(VideoLoadedListener listener, Context context) {
        this.context = context;
        this.listener = listener;
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://www.googleapis.com/")
                .build();

        youtubeService = retrofit.create(YoutubeService.class);
    }

    public interface YoutubeService {

        @GET("youtube/v3/search")
        Call<ResponseBody> load(@Query("part") String part,
                                @Query("maxResults") String maxResults,
                                @Query("order") String order,
                                @Query("publishedAfter") String publishedAfter,
                                @Query("key") String apiKey);
    }

    public void loadVideos(){

        String part = "snippet";
        String maxResults = "2";
        String order = "viewCount";
//        later bind to current date
        String publishedAfter = "2018-05-25T00:00:00Z";
        String apiKey = context.getString(R.string.youtube_api_key);

        Call<ResponseBody> call = youtubeService.load(part, maxResults, order, publishedAfter, apiKey);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    JSONArray items = jsonObject.getJSONArray("items");
                    for (int i = 0; i < items.length(); i++){
                        String videoId = items.getJSONObject(i).getJSONObject("id").getString("videoId");
                        listener.onVideoLoaded(new VideoItem(videoId, VIDEO_SERVICE.YOUTUBE, null));
                    }
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(LOG_TAG, t.getMessage());
                Log.e(LOG_TAG, call.request().toString());
            }
        });
    }
}
