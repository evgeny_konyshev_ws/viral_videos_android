package ru.worksolutions.viralvideos;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.dailymotion.android.player.sdk.PlayerWebView;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerFragment;
import com.google.android.youtube.player.YouTubeThumbnailLoader;
import com.google.android.youtube.player.YouTubeThumbnailView;

import java.util.List;

public class VideoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String LOG_TAG = VideoAdapter.class.getSimpleName();
    private static final int STUB = 0;
    private static final int YOUTUBE_SCREEN = 1;
    private static final int VIMEO_SCREEN = 2;
    private static final int DAILYMOTION_SCREEN = 3;

    private boolean readyForLoadingYoutubeThumbnail = true;

    private Context context;
    private List<VideoItem> videoItems;
    private int containerOldId;
    private ImageView oldThumbNail;

    public VideoAdapter(Context context, List<VideoItem> videoItems) {
        this.context = context;
        this.videoItems = videoItems;
    }

    @Override
    public int getItemCount() {
        return videoItems.size();
    }

    public void add(VideoItem videoItem) {
        videoItems.add(videoItem);
        Log.e(LOG_TAG, videoItems.toString());
        notifyDataSetChanged();
    }

    public void clear() {
        videoItems.clear();
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        if (position == videoItems.size()){
            return STUB;
        } else {
            return getItemViewTypeFromVideoType(videoItems.get(position));
        }
    }

    private int getItemViewTypeFromVideoType(VideoItem videoUrl){
        if (videoUrl.getVideoServise().equals(VIDEO_SERVICE.VIMEO)){
            return VIMEO_SCREEN;
        } else if (videoUrl.getVideoServise().equals(VIDEO_SERVICE.YOUTUBE)){
            return YOUTUBE_SCREEN;
        } else {
            return DAILYMOTION_SCREEN;
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType){
            case YOUTUBE_SCREEN:
                return new YoutubeViewHolder(inflater.inflate(R.layout.youtube_view_holder, parent, false));
            case VIMEO_SCREEN:
                return new VimeoViewHolder(inflater.inflate(R.layout.vimeo_view_holder, parent, false));
            case DAILYMOTION_SCREEN:
                return new DailymotionViewHolder(inflater.inflate(R.layout.dailymotion_view_holder, parent, false));
            case STUB:
                return new StubViewHolder(inflater.inflate(R.layout.stub_view_holder, parent, false));
            default:
                throw new AssertionError();
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof YoutubeViewHolder){
            ((YoutubeViewHolder)holder).bind(videoItems.get(position).getId());
        } else if (holder instanceof VimeoViewHolder){
            ((VimeoViewHolder)holder).bind(videoItems.get(position).getId());
        } else if (holder instanceof DailymotionViewHolder){
            ((DailymotionViewHolder)holder).bind(videoItems.get(position).getId(), videoItems.get(position).getThumbnailUrl());
        }
    }

    interface VideoShutter {
        void closeCurrentlyPlayingVideo();
    }

    class YoutubeViewHolder extends RecyclerView.ViewHolder implements VideoShutter{

        private YouTubeThumbnailView youTubeThumbnailView;
        private FrameLayout fragmentContainer;
//        change
        private int youTubeContainerNewId = (int)(Math.random()*100);
        private FragmentManager fragmentManager;

        YoutubeViewHolder(final View itemView) {
            super(itemView);
            fragmentManager = ((Activity)context).getFragmentManager();
            youTubeThumbnailView = itemView.findViewById(R.id.youtube_thumbnail);
            fragmentContainer = itemView.findViewById(R.id.youtube_container);
            fragmentContainer.setId(youTubeContainerNewId);
        }

        @Override
        public void closeCurrentlyPlayingVideo() {
//            if (containerOldId != 0) {
//                fragmentManager.beginTransaction().remove(fragmentManager.findFragmentById(containerOldId)).commit();
//                oldThumbNail.setVisibility(View.VISIBLE);
//            }
        }

        void bind(final String videoUrl){

            closeCurrentlyPlayingVideo();

            containerOldId = fragmentContainer.getId();
            oldThumbNail = youTubeThumbnailView;

            if (readyForLoadingYoutubeThumbnail) {
                readyForLoadingYoutubeThumbnail = false;
                youTubeThumbnailView.initialize(context.getString(R.string.youtube_api_key), new YouTubeThumbnailView.OnInitializedListener() {
                    @Override
                    public void onInitializationSuccess(YouTubeThumbnailView youTubeThumbnailView, final YouTubeThumbnailLoader youTubeThumbnailLoader) {
                        youTubeThumbnailLoader.setVideo(videoUrl);
                        youTubeThumbnailLoader.setOnThumbnailLoadedListener(new YouTubeThumbnailLoader.OnThumbnailLoadedListener() {

                            @Override
                            public void onThumbnailLoaded(YouTubeThumbnailView childYouTubeThumbnailView, String s) {
                                childYouTubeThumbnailView.setVisibility(View.VISIBLE);
                                youTubeThumbnailLoader.release();
                            }

                            @Override
                            public void onThumbnailError(YouTubeThumbnailView youTubeThumbnailView, YouTubeThumbnailLoader.ErrorReason errorReason) {
                                youTubeThumbnailLoader.release();
                            }
                        });

                        readyForLoadingYoutubeThumbnail = true;
                    }

                    @Override
                    public void onInitializationFailure(YouTubeThumbnailView youTubeThumbnailView, YouTubeInitializationResult error) {
                        Log.e(LOG_TAG, "onInitializationFailure()" + error.toString());
                        Toast.makeText(context, error.toString(), Toast.LENGTH_LONG).show();
                    }
                });
            }

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    closeCurrentlyPlayingVideo();

                    youTubeThumbnailView.setVisibility(View.GONE);
                    final YouTubePlayerFragment youTubePlayerFragment = YouTubePlayerFragment.newInstance();
                    fragmentManager.beginTransaction().add(fragmentContainer.getId(), youTubePlayerFragment).commit();
                    youTubePlayerFragment.initialize(context.getString(R.string.youtube_api_key), new YouTubePlayer.OnInitializedListener() {
                        @Override
                        public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
                            youTubePlayer.loadVideo(videoUrl);
                        }

                        @Override
                        public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult error) {
                            Toast.makeText(context, error.toString(), Toast.LENGTH_LONG).show();
                        }
                    });
                }
            });
        }
    }

    class VimeoViewHolder extends RecyclerView.ViewHolder{

        private WebView webView;

        public VimeoViewHolder(View itemView) {
            super(itemView);
            webView = itemView.findViewById(R.id.vimeo_web_view);
        }

        void bind(String videoUrl) {
            webView = itemView.findViewById(R.id.vimeo_web_view);
            webView.setWebViewClient(new WebViewClient());
            webView.getSettings().setJavaScriptEnabled(true);
//                webView.getSettings().setAppCacheEnabled(true);
//                webView.getSettings().setDomStorageEnabled(true);
            webView.loadUrl(videoUrl);
        }
    }

    class DailymotionViewHolder extends RecyclerView.ViewHolder implements VideoShutter{

        private PlayerWebView playerWebView;
        private ImageView thumbnail;
        private boolean isCurrentlyPlaying;
        private int dmContainerNewId = (int)(Math.random()*100);

        public PlayerWebView getPlayerWebView() {
            return playerWebView;
        }

        public DailymotionViewHolder(View itemView) {
            super(itemView);
            playerWebView = itemView.findViewById(R.id.dm_player_web_view);
            thumbnail = itemView.findViewById(R.id.dailymotion_thumbnail);
            playerWebView.setId(dmContainerNewId);
        }

        void bind(final String videoUrl, String thumbnailUrl) {

            closeCurrentlyPlayingVideo();

            containerOldId = playerWebView.getId();
            oldThumbNail = thumbnail;

            if (thumbnailUrl != null) {
                Glide
                        .with(context)
                        .load(thumbnailUrl)
                        .asBitmap()
                        .into(thumbnail);
            }

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
    //            don't forget to manage lifecycle
                    thumbnail.setVisibility(View.GONE);
                    Log.e(LOG_TAG, videoUrl);
                    playerWebView.load(videoUrl);
                }
            });
        }

        @Override
        public void closeCurrentlyPlayingVideo() {
            if (containerOldId != 0){
                PlayerWebView playerWebView = ((Activity)context).findViewById(containerOldId);
                playerWebView.stopLoading();
                oldThumbNail.setVisibility(View.VISIBLE);
            }
        }
    }

    class StubViewHolder extends RecyclerView.ViewHolder{

        ProgressBar progressBar;

        public StubViewHolder(View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.stub_progress_bar);
        }
    }
}
