package ru.worksolutions.viralvideos;

public class VideoItem {
    private String id;
    private VIDEO_SERVICE videoServise;
    private String thumbnailUrl;

    public VideoItem(String id, VIDEO_SERVICE videoServise, String thumbnailUrl) {
        this.id = id;
        this.videoServise = videoServise;
        this.thumbnailUrl = thumbnailUrl;
    }

    public String getId() {
        return id;
    }

    public VIDEO_SERVICE getVideoServise() {
        return videoServise;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }
}
